function checker(details, RR_number,quantity) {

    for(var record = 0; record<details.recordset.length; record++){
      if(RR_number == "" && quantity == ""){
        RR_number += details.recordset[record]['part_num'];
        quantity += details.recordset[record]['qty'];
      } else{
        RR_number = RR_number + " " + details.recordset[record]['part_num'];
        quantity += " " + details.recordset[record]['qty'];
      }
    }
    return [RR_number,quantity];
}

module.exports = checker;
